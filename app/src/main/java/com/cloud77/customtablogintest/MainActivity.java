package com.cloud77.customtablogintest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private CallbackManager fbCallbackManager;
    private Context context;
    private LoginManager fbLoginManager;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        fbCallbackManager.onActivityResult( requestCode, resultCode, data );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        FacebookSdk.sdkInitialize(getApplicationContext());
        fbCallbackManager = CallbackManager.Factory.create();
        fbLoginManager = LoginManager.getInstance();

        findViewById( R.id.activity_main_login_button ).setOnClickListener(
            new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    fbLoginManager.registerCallback(
                        fbCallbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                fbLoginManager.logOut();
                            }

                            @Override
                            public void onCancel() {
                                fbLoginManager.logOut();
                            }

                            @Override
                            public void onError(FacebookException error) {
                                error.printStackTrace();
                                fbLoginManager.logOut();
                            }
                        }
                    );

                    fbLoginManager.setLoginBehavior(LoginBehavior.WEB_ONLY );
                    ArrayList<String> permissionArrayList = new ArrayList<String>();

                    permissionArrayList.add( "public_profile" );
                    fbLoginManager.logInWithReadPermissions( ( MainActivity )context, permissionArrayList );
                }
            }
        );
    }
}
